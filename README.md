BerkleyNode
===================

* Created on Sun May 24, 2015 *
* Created by Arjun UID 504078752 for ER LAB @ UCLA *

ABOUT
-----

This software ensures that the rPi nodes are running the latest version
of the code. This is done by pulling the latest code from BitBucket Git
every 5 minutes. It also pushes the latest APK from the server to the
watches. 

INSTALLATION INSTRUCTIONS
-------------------------

Prerequesities ::

1. OpenVPN Client installed
2. ADB version >= 1.30
3. All connected watches must have debugging enabled and authorized
4. SFTP server running on server-side
5. SSH access to server
6. SUDO access

For completely automated access, you will also need ::

1. Certificates and keys for VPN access
2. Keys for SSH access
3. Keys for auto-pulling from git
--------------------------------------------------------------------
I. Once all of the above have been installed, then git clone SRC_DIR
   to /home/pi/.syncService.

II. Setup a SUDOer's crontab to look like :: 
    * * * * * /home/pi/.syncService/bin/codeSync
--------------------------------------------------------------------

This ensures that 'codeSync' synces everything, including pulling
all updates from the server, running the watch sync, and server sync
functions.

Infact, try and get hold of a system image file for the raspberry pi.
This is the quickest way to install the entire application. 

